# ansible

#### 介绍
ansible自动化部署[centos7]（jdk、elasticsearch、fastdfs、mysql56[双主、主从]、mysql57[双主、主从]、mysql8[双主、主从]、redis哨兵、rocketmq[双主无从、双主双从]、zookeeper集群、kafka集群、rabbitmq镜像集群、mongodb副本集、minio分布式集群）

#### 1下载软件包并且安装好ansible
```
链接: https://pan.baidu.com/s/13HoUizYWtLEH1ACbnKZQCA?pwd=xpcj 提取码: xpcj
```
链接失效请留言！！
#### 2修改软件包的解压位置
```yaml
vim group_vars/all.yml
...
src_dir : '/soft/path'
...
```
#### 3修改主机(对应修改自己需要部署的组)
```
vim hosts
...
[zookeeper]
10.255.64.63 myid=0
10.255.64.64 myid=1
10.255.65.60 myid=2
...
```
#### 4安装(xxx.yml替换成自己部署的集群服务)
```
ansible-playbook -i hosts xxx.yml -uroot -k
```
